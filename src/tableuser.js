import React, { Component } from 'react';
import 'antd/dist/antd.css';
import './css/navbar.css';
import './flareg.ico';
import axios from 'axios';
import Keywordlist from './keywordlist';
import { Table, message, Icon} from 'antd';
import { connect } from 'react-redux';
import { actionSelectUser } from './keeperAction';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';


// const data = [{
//   key: '1',
//   rank: '1',
//   name: 'ba546f8d6d55634ce9106423ee4c5275',
//   frequency: 20,
//   action: 'ba546f8d6d55634ce9106423ee4c5275',
// }, {
//   key: '2',
//   rank: '2',
//   name: '4535367f2f39b5a2ebaee0092f184a79',
//   frequency: 15,
//   action:'4535367f2f39b5a2ebaee0092f184a79',
// }, {
//   key: '3',
//   rank: '3',
//   name: 'b723ec00f1a277fc71e5644eea40af38',
//   frequency: 5,
//   action:'b723ec00f1a277fc71e5644eea40af38',
// }, {
//   key: '4',
//   rank: '4',
//   name: '27090706d42a2525b9a07222f68dd3d4',
//   frequency: 3,
//   action:'27090706d42a2525b9a07222f68dd3d4',
// }];

function onChange(pagination, filters, sorter) {
  console.log('params', pagination, filters, sorter);
}

class TableUser extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }



  receiveUser = (data) => {
    for(var k in this.props.formatData){
      if(this.props.formatData[k].name === data){
          var userid = this.props.formatData[k].id 
      }
    }
    message.info(`'${data}' was selected!`);
    this.props.actionSelectUser(userid)
    console.log(this.props.selectUser)
  }

  onRowClick(record) {
    const expandedRowKeys = this.state.expandedRowKeys;

  }

  render() 
  {
    const columns = [{
      title: 'Rank',
      dataIndex: 'rank',
      defaultSortOrder: 'ascend',
      sorter: (a, b) => a.rank - b.rank,
      align: 'center',
    },{
      title: 'User Name',
      dataIndex: 'name',
      onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.name.length - b.name.length,
      render: text => <a onClick={() => this.receiveUser(text)}>{text}</a>,
    }, {
      title: 'Frequency',
      dataIndex: 'frequency',
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.frequency - b.frequency,
      align: 'center',
    }, {
      title: 'Action',
      dataIndex: 'action',
      key: 'action',
      align: 'center',
      render: text => <Link to={"/user"+"?"+this.props.selectUser} onClick ={() => this.receiveUser(text)}><Icon type="eye" style={{ fontSize: 20, color: '#08c' }} /></Link>,
  },
  ];
    return (
      <div>
        <Table columns={columns} dataSource={this.props.formatData} onChange={this.handleChange} size="small" style={{boxShadow: 'rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px',borderRadius: 10,background: '#fff' }} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  actionSelectUser: (data) => dispatch(actionSelectUser(data)),
});


const mapStateToProps = (state) => ({
  selectUser: state.Keeper.selectUser,
});

export default connect(mapStateToProps,mapDispatchToProps)(TableUser);