import {combineReducers} from 'redux';

import Keeper from './keeperReducer';

export default combineReducers({
    Keeper
});