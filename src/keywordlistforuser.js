import React, { Component } from 'react';
import 'antd/dist/antd.css';
import './css/navbar.css';
import './css/keywordblink.css';
import './flareg.ico';
import { connect } from 'react-redux';

import { TagCloud } from "react-tagcloud";
import { Slider, InputNumber, Row, Col, message } from 'antd';
import {  actionSelectKeyword, actionSliderUser } from './keeperAction';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

//const colorCustom = ["#3d3d3d", "#3ae374", "#ff3838", "#7158e2", "#ffff", "#2c3e50", "#f1c40f", "#2980b9", "#bdc3c7", "#c56cf0"]

const customRenderer = (tag, size, colorCustom) => {
  console.log(colorCustom)
  return (<span className="bounceTags"
    style={{
      fontSize: `${size}em`,
      border: `2px solid #000`,
      borderRadius: '5px',
      margin: '3px',
      padding: '3px',
      display: 'inline-block',
      color: 'rgb(0, 22, 41)',
      cursor: 'pointer',
      boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
    }}><Link to="/keyword" style={{color:'#000'}}>{tag.value}</Link></span>)

  };

class Keywordlistforuser extends Component {
  constructor(props) {
    super(props)
    this.state = {
      inputValue: this.props.sliderKeywordUser,
    }
  }


  onChange = (value) => {
    /* this.setState({
      inputValue: value,
    }); */
    this.setState({
      inputValue: value
    })
    this.props.actionSliderUser(value);
    this.props.getNewValuetop(value);
  }

  receive = (tag) => {
    message.info(`'${tag.value}' was selected!`);
    this.props.actionSelectKeyword(tag.value)
  };


  render() {
    // this.props.actionSelectKeyword("test my word");
    //console.log(this.props.selectKeyword);
    console.log(this.props.usertopList);
    return (
      <div>
        <Row style={{margin:'0 0 20px 0',}}>
          <Col span={21}>
            <Slider onChange={this.onChange} value={this.state.inputValue} />
          </Col>
          <Col span={2}>
            <InputNumber
              style={{ marginLeft: 16 }}
              value={this.state.inputValue}
              onChange={this.onChange}
            />
          </Col>
        </Row>
        
       <TagCloud onClick={tag => this.receive(tag)}
          tags={this.props.usertopList}
          minSize={1}
          maxSize={2}
          renderer={customRenderer}></TagCloud> 
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  
  actionSelectKeyword: (data) => dispatch(actionSelectKeyword(data)),
  actionSliderUser: (data) => dispatch(actionSliderUser(data)),
  
});


const mapStateToProps = (state) => ({
  
  selectKeyword: state.Keeper.selectKeyword,
  sliderKeywordUser: state.Keeper.sliderKeywordUser,
 

});


export default connect(mapStateToProps, mapDispatchToProps)(Keywordlistforuser)
