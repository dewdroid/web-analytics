import React, { Component } from 'react';
import 'antd/dist/antd.css';
import './css/navbar.css';
import './flareg.ico';
import 'c3/c3.css';
import { PieChart, Pie, Legend, Tooltip ,Cell ,ResponsiveContainer} from 'recharts';
const COLORS = ['#16BAC5', '#5863F8', '#171D1C', '#5FBFF9'];


const data = [{}, {}, {}];


class Chartpie extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }

  render() {
    return (  
      <ResponsiveContainer height={400}>
      <PieChart redraw>
        <Pie
          data={this.props.dataList}
          dataKey="value"
          key="key"
          label={true}
          fill="#8884d8"
        >
          {
            data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
          }
        </Pie>
        <Legend verticalAlign="bottom" height={36}/>
        <Tooltip/>
      </PieChart>
      </ResponsiveContainer>
    );
  }
}

export default Chartpie;
