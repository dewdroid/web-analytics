import React, { Component } from 'react';
import 'antd/dist/antd.css';
import './css/navbar.css';
import { Table, Button, message, Icon, Input } from 'antd';
import { connect } from 'react-redux';
import { actionSliderValue, actionSelectKeyword } from './keeperAction';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';


// const data = [{
//   key: '1',
//   rank: 1,
//   kname: 'mysql',
//   frequency: 42,
//   action: 'mysql',

// }, {
//   key: '2',
//   rank: 3,
//   kname: 'tree',
//   frequency: 29,
//   action: 'tree',

// }, {
//   key: '3',
//   rank: 2,
//   kname: 'python',
//   frequency: 32,
//   action: 'python',

// }, {
//   key: '4',
//   rank: 4,
//   kname: 'ruby',
//   frequency: 15,
//   action: 'ruby',

// }, {
//   key: '5',
//   rank: 5,
//   kname: 'rail',
//   frequency: 13,
//   action: 'rail',

// }, ];

class TableTopKey extends Component {


  constructor(props) {
    super(props)
    this.state = {
      filterDropdownVisible: false,
      searchText: '',
      filtered: false,
      data: [],
    }
  }

  componentWillMount = ()=> {
    this.setState({
      data: this.props.formatData
    })
  }

  onInputChange = (e) => {
    this.setState({ searchText: e.target.value });
  }
  onSearch = () => {
    const { searchText } = this.state;
    const reg = new RegExp(searchText, 'gi');
    this.setState({
      filterDropdownVisible: false,
      filtered: !!searchText,
      data: this.props.formatData.map((record) => {
        const match = record.value.match(reg);
        //console.log(match)
        if (!match) {
          return null;
        }
        return {
          ...record,
          value: (
            <span>
              {record.value.split(reg).map((text, i) => (
                i > 0 ? [<span className="highlight">{match[0]}</span>, text] : text
              ))}
            </span>
          ),
        };
      }).filter(record => !!record),
    });
    
  }

  receive = (data) => {
    message.info(`'${data}' was selected!`)
    this.props.actionSelectKeyword(data)

}


  render() {

    //console.log(this.state.data);
    const columns = [{
      title: 'Ranking',
      dataIndex: 'index',
      key: 'index',
      defaultSortOrder: 'ascend',
      sorter: (a, b) => a.index - b.index,
      width: '25%',
      align: 'center',
    }, {
      title: 'Keyword',
      dataIndex: 'value',
      key: 'value',
      width: '50%',
      filterDropdown: (
        <div className="custom-filter-dropdown">
          <Input
            ref={ele => this.searchInput = ele}
            placeholder="Search Keyword"
            value={this.state.searchText}
            onChange={this.onInputChange}
            onPressEnter={this.onSearch}
          />
          <Button type="primary" onClick={this.onSearch}>Search</Button>
        </div>
      ),
      filterIcon: <Icon type="search" style={{ color: this.state.filtered ? '#108ee9' : '#aaa' }} />,
      filterDropdownVisible: this.state.filterDropdownVisible,
      onFilterDropdownVisibleChange: (visible) => {
        this.setState({
          filterDropdownVisible: visible,
        }, () => this.searchInput && this.searchInput.focus());
      },
    }, 
    {
      title: 'Action',
      dataIndex: 'action',
      key: 'action',
      width: '25%',
      align: 'center',
      render: text => <Link to="/keyword" onClick={() => this.receive(text)}><img src={require('./pic/curvechart2.png')} alt="Smiley face" width="16" height="16" /></Link>,
    }];

    return (
      <div>
        <Table  pagination={{ pageSize: 10 }} columns={columns} dataSource={this.state.searchText === '' ? this.props.formatData : this.state.data} size="small" style={{ boxShadow: 'rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px', borderRadius: 10, background: '#fff' }} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  actionSelectKeyword: (data) => dispatch(actionSelectKeyword(data)),
});


const mapStateToProps = (state) => ({
  selectKeyword: state.Keeper.selectKeyword,
});

export default connect(mapStateToProps, mapDispatchToProps)(TableTopKey);