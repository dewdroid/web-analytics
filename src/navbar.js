import React , { Component } from 'react';
import 'antd/dist/antd.css';
import './css/navbar.css';
import './flareg.ico';
import { Layout, Menu, Icon } from 'antd';
import Mainpage from './mainpage';
import Mainkeyword from './mainkeyword';
import User from './user';
import Categories  from './categories';
import Radium from 'radium';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
} from 'react-router-dom';
import Redirect from 'react-router-dom/Redirect';
import categories from './categories';
const { Sider } = Layout;

export const LayoutStyle = {
  minHeight: '100vh',
};

class Navbar extends Component {

  state = {
    collapsed: false,
    current: '1',
  };
  onCollapse = (collapsed) => {
    // console.log(collapsed);
    this.setState({ collapsed });
  }

  render() {

    return (
      <Router>
        <Layout style={LayoutStyle}>
          <Sider
            collapsedWidth="0"
            breakpoint="sm"
            collapsible
            collapsed={this.state.collapsed}
            onCollapse={this.onCollapse}
          >
            <div className="logo animated bounceInLeft" ><img src="flareg.ico" />Word</div>

            <Menu theme="dark" mode="inline" selectedKeys={['one']} defaultOpenKeys={['one']} >

              <Menu.Item key="one">
                <Link to="/">
                  <Icon type="pie-chart" />
                  <span>Overview</span>
                </Link>
              </Menu.Item>

              <Menu.Item key="two" >
                <Link to="/keyword">
                  <Icon type="medium" />
                  <span>Observe Keyword</span>
                </Link>
              </Menu.Item>

              <Menu.Item key="three">
                <Link to="/user">
                  <Icon type="user" />
                  <span>Observe User</span>
                </Link>
              </Menu.Item>
            </Menu>
          </Sider>
          <Switch>
            <Route exact path="/" component={Mainpage} />
            <Route path="/keyword" component={Mainkeyword} />
            <Route path="/user" component={User} />
            <Route path="/create_categories" component={categories} />
            <Redirect to="/" />
          </Switch>
        </Layout>
      </Router>
    );
  }
}

export default Radium(Navbar);
