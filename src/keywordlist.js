import React, { Component } from 'react';
import 'antd/dist/antd.css';
import './css/navbar.css';
import './css/keywordblink.css';
import './flareg.ico';
import { connect } from 'react-redux';

import { TagCloud } from "react-tagcloud";
import { Slider, InputNumber, Row, Col, message } from 'antd';
import { actionSliderValue, actionSelectKeyword } from './keeperAction';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';


const customRenderer = (tag, size, color) => (
  <span key={tag.value} className="bounceTags"
    style={{
      fontSize: `${size}em`,
      border: `2px solid ${color}`,
      borderRadius: '5px',
      margin: '3px',
      padding: '3px',
      display: 'inline-block',
      color: 'rgb(0, 22, 41)',
      cursor: 'pointer',
      boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
    }}><Link to="/keyword" style={{color:'#000'}}>{tag.value}</Link></span>

);

class Keywordlist extends Component {
  constructor(props) {
    super(props)
    this.state = {
      inputValue: this.props.sliderKeyword,
    }
  }

  onChange = (value) => {

   
    this.setState({
      inputValue: value
    })
    this.props.actionSliderValue(value);
    this.props.getNewValuetop(value);

  }

  receive = (tag) => {
    message.info(`'${tag.value}' was selected!`);
    this.props.actionSelectKeyword(tag.value);
    // console.log(tag)
    //console.log(tag.value)
  }


  render() {
    // this.props.actionSelectKeyword("test my word")
    // console.log(this.props.selectKeyword)
    //console.log(this.props.sliderKeyword)
    return (
      <div>
        <div className="sliderKeyword">
        <Row>
          <Col span={20}>
            <Slider onChange={this.onChange} value={this.state.inputValue} />
          </Col>
          <Col span={3}>
            <InputNumber

              style={{ marginLeft: 16 }}
              value={this.state.inputValue}
              onChange={this.onChange}
            />
          </Col>
        </Row>
        </div>
        <TagCloud onClick={tag => this.receive(tag)}
          tags={this.props.topList}
          minSize={1}
          maxSize={3}
          renderer={customRenderer}></TagCloud>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  actionSliderValue: (data) => dispatch(actionSliderValue(data)),
  actionSelectKeyword: (data) => dispatch(actionSelectKeyword(data)),
});


const mapStateToProps = (state) => ({
  sliderKeyword: state.Keeper.sliderKeyword,
  selectKeyword: state.Keeper.selectKeyword,

});


export default connect(mapStateToProps, mapDispatchToProps)(Keywordlist)
