import { DatePicker, Calendar } from 'antd';
import React, { Component } from 'react';
import axios from 'axios';
import 'antd/dist/antd.css';
import './css/navbar.css';
import moment from 'moment';
import { serverUrlNew } from './mainpage';

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const defaultDateStart = "2017-11-16";
const defaultDateEnd = "2018-01-15";

class Datefilter extends Component {
  constructor() {
    super()
    this.state = {
      dataDate: [],
      
    }
  }

  componentWillMount = () => {
    this.getValue()
    this.getValuetop()
  }

  onChangeDate = (value, dateString) => {
    console.log("StartDate: " + dateString[0])
    console.log("EndDate: " + dateString[1])
    //  /page/top-keyword/?before=2017-11-16&after=2018-01-15
    axios.get(serverUrlNew + "/page/top-keyword/?before=" + dateString[0] + "&after=" + dateString[1])
      .then(data => {
        this.setState({ dataDate : data.data })
        console.log(data.data);
      })
  }

  onOk = (value) => {
    console.log('onOk: ', value);
  }

  render() {
    return (
      <div>
        <RangePicker
          onOk={this.onOk}
          format="YYYY-MM-DD"
          placeholder={['Start Time', 'End Time']}
          onChange={this.onChangeDate}
          value={[moment("2017-11-16"),moment("2018-01-15")]}
        />
      </div>
    );
  }
}

export default Datefilter;
