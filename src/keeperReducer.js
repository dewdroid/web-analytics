const initialState = {
    sliderKeyword: 50,
    sliderKeywordUser: 10,
    selectKeyword: "",
    selectUser: "",
    allDate: [],
    startDate: "2018-1-15",
    endDate: "2018-01-15",
    userKeyword: [],
    allUserKeyword: [],
};

const KeeperReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'CHANGE_DATA':
            console.log('reducer here')
            console.log(action.sliderValue)
            return Object.assign({}, state, { sliderKeyword: action.sliderValue });

        case 'CHANGE_KEYWORD':
            console.log('reducer2 here')
            console.log(action.keywordCloud)
            return Object.assign({}, state, { selectKeyword: action.keywordCloud });

        case 'CHANGE_USER':
            console.log('reducer3 here')
            console.log(action.getUser)
            return Object.assign({}, state, { selectUser: action.getUser });

        case 'CHANGE_DATA_USER':
            console.log('reducer4 here')
            console.log(action.sliderUser)
            return Object.assign({}, state, { sliderKeywordUser: action.sliderUser });

        case 'CHANGE_DATE':
            console.log('reducer5 here')
            console.log(action.getAllDate)
            return Object.assign({}, state, { allDate: action.getAllDate });

        case 'CHANGE_DATE_START':
            console.log('reducer6 here')
            console.log(action.getStartDate)
            return Object.assign({}, state, { startDate: action.getStartDate });

        case 'CHANGE_DATE_END':
            console.log('reducer7 here')
            console.log(action.getEndDate)
            return Object.assign({}, state, { endDate: action.getEndDate });

        case 'CHANGE_UNAME':
            console.log('reducer8 here')
            console.log(action.getDataUser)
            return Object.assign({}, state, { userKeyword: action.getDataUser });

        case 'CHANGE_DATA_KEYWORD_USER':
            console.log('reducer9 here')
            console.log(action.getKeywordUser)
            return Object.assign({}, state, { allUserKeyword: action.getKeywordUser });

        default:
            return state;
    }
};

export default KeeperReducer;