import React, { Component } from 'react';
import 'antd/dist/antd.css';
import './css/navbar.css';
import Radium from 'radium';
import axios from 'axios';
import { Layout, Row, Col, Checkbox, Button, Icon } from 'antd';
import { serverUrlNew } from './mainpage';


const { Content, Footer, } = Layout;

export const navStyle = {
  base: {
    backgroundColor: '#eff1f4',
    marginLeft: -31,
    marginRight: -46,
    backgroundColor: '#001629',
    marginTop: -16,
    minHeight: 55,
    minWidth: 'fit-content',/* position:  absolute; */
    paddingLeft: 30,
    borderRadius: 5,
    border: '1px solid #ccc',
    transition: '2s',
  },
  card: {
    backgroundColor: '#fff',
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
    borderRadius: '5px 5px 5px 5px',
    minWidth: 500,
    margin: 'auto',
    padding: '1px',
    marginTop: '20px',
    overflow: 'hidden',
    padding: 20,

  },
  content: {
    backgroundColor: '#fff',
    textAlign: 'center',
    fontSize: '30px',

  },
  web: {
    backgroundColor: '#fff',
    textAlign: 'center',
    fontSize: '16px',
  },
  link: {
    wordWrap: 'break-word',
  },
  send_button: {
    width: 300,
    margin: '20px 0 20px 0',
  },
  remain: {
    fontSize: '18px',
  },

};


class Categories extends Component {
  constructor() {
    super()
    this.state = {
      selected: [],
      checked_1: false,
      checked_2: false,
      pages: [],
      pages_id: [],
      remain: [],
    }
  }

  componentWillMount = () => {
    this.importPage()
  }

  importPage = () => {

    axios.get(serverUrlNew + "/page/show-category/")
      .then(data => {
        this.setState({
          pages: data.data.page,
          pages_id: data.data.page_id,
          remain: data.data.remain
        },() => { console.log('new state', this.state.remain); })
        
      })

  }

  // setRemain = ()=> {
  //   this.setState( previous =>({
  //       remain: previous.remain
  //   }))
  //   console.log(this.state.remain)
  // }

  onChange = (checkedValues) => {
    console.log('checked = ', checkedValues);

    this.setState({
      selected: checkedValues
    })

    if (checkedValues === 1) {
      this.setState({
        checked_1: true,
        checked_2: false,
      })
    } else {
      this.setState({
        checked_1: false,
        checked_2: true,
      })
    }
    console.log(this.state.selected);
  }

  onClick = () => {

    axios.post(serverUrlNew + "/page/make-category/", {
      page_id: this.state.pages_id,
      is_technology: this.state.selected
    })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });

    this.importPage()
  }

  onRe = () => {
    this.importPage()
  }



  render() {
    return (
      <Layout>
        <Content style={{ margin: '0 16px' }}>
          <div style={{ padding: 24, background: '#f0f2f5', minHeight: 360 }}>
            <Row gutter={16} style={navStyle.base}>
              <Col xs={{ span: 24 }} md={{ span: 24 }} style={{ minHeight: 30, fontSize: 32, textAlign: 'left', color: '#ffff' }}>Create Category</Col>
            </Row>
            <div style={navStyle.card}>
              <Row gutter={16} type="flex" justify="center">
                <Col style={navStyle.content}>
                  คลิกที่ลิงค์เพื่อเข้าสำรวจ <Icon className="re_button" onClick={this.onRe} type="sync" />
                </Col>
              </Row>

              <Row gutter={16} type="flex" justify="space-around" style={{ marginTop: '20px' }}>
                <Col xs={{ span: 20 }} md={{ span: 15 }} style={navStyle.web}>
                  <a style={navStyle.link} href={this.state.pages} target="_blank">{this.state.pages}</a>
                </Col>
              </Row>

              <Row gutter={16} type="flex" justify="space-around" style={{ marginTop: '20px' }}>
                <Col style={navStyle.content}>
                  ท่านคิดว่าเป็นเว็บเพจในหมวดหมู่ใด
              </Col>
              </Row>

              <Row gutter={16} type="flex" justify="space-around" >
                <Col style={navStyle.content}>
                  <Checkbox onChange={() => this.onChange(1)}
                    checked={this.state.checked_1}

                  >Technology
              </Checkbox>
                  <Checkbox onChange={() => this.onChange(0)}
                    checked={this.state.checked_2}

                  >Non-Technology
                  </Checkbox>
                </Col>
              </Row>

              <Row gutter={16} type="flex" justify="space-around" >
                <Col style={navStyle.remain}>
                  เพจที่เหลือ : {this.state.remain}
                </Col>
              </Row>

              <Row gutter={16} type="flex" justify="space-around" >
                <Col style={navStyle.content}>
                  <Button style={navStyle.send_button} onClick={this.onClick} type="primary" size="large">Send</Button>
                </Col>
              </Row>

            </div>
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          <div className="animated fadeInUp">FlareViz Design ©2017 Created by CE6039</div>
        </Footer>
      </Layout>
    );
  }
}

export default Categories;
