import React, { Component } from 'react';
import 'antd/dist/antd.css';
import './css/navbar.css';
import './flareg.ico';
import axios from 'axios';
import Keywordlist from './keywordlist';
import LineChartKeyword from './linechartKeyword';
import { serverUrlNew } from './mainpage';
import TableUser from './tableuser';
import moment from 'moment';
import { Layout, Row, Col, Button, Tooltip, DatePicker} from 'antd';
import { connect } from 'react-redux';
import { actionStartDate, actionEndDate, actionAllDate, actionSelectKeyword, actionDataUser } from './keeperAction';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

const { Content, Footer, Sider } = Layout;
const { RangePicker } = DatePicker;


export const navStyleKeyword = {
  base: {
    backgroundColor: '#eff1f4',
    marginLeft: -31,
    marginRight: -46,
    backgroundColor: '#001629',
    marginTop: -16,
    minHeight: 55,
    minWidth: 'fit-content',/* position:  absolute; */
    paddingLeft: 30,
    borderRadius: 5,
    border: '1px solid #ccc',
    transition: '2s',
  },
  keycome: {
    display: 'inline-block', color: '#5fbff8',
    textDecoration: 'underline',
    textDecorationStyle: 'dotted'
  },
  keywordlistDate: {
    minHeight: 'auto',
    textAlign: 'center',
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
    transition: 'all 0.3s cubic-bezier(.25, .8, .25, 1)',
    padding: 20,
    marginBottom: 20,
    borderRadius: 10,
    background: '#48acf3',
    borderTopColor: 'coral',

  }
};

class Mainkeyword extends Component {
  constructor() {
    super()
    this.state = {
      topList: [],
      dataList: [],
      sites: [],
      StartDate: "",
      EndDate: "",
      dataLine: [],
      dataTable: [],
      formatData: [],
    }
  }

  componentWillMount = () => {
    this.getLineChart()
    this.getTableUser()
  }

  onClick = () => {
    // console.log("StartDate3: " + this.state.StartDate)
    // console.log("EndDate3: " + this.state.EndDate)

    //http://161.246.35.220:9000/page/top-keyword/?before=2017-11-16&after=2018-01-15
    axios.get(serverUrlNew + "/page/top-keyword/?before=" + this.props.startDate + "&after=" + this.props.endDate)
      .then(data => {
        this.props.actionAllDate(data.data)
        this.getLineChart()
        this.getTableUser()
       // console.log(this.props.allDate);
        // console.log(topList)
      }).catch(error => {
          console.log(error)
      });

  }

  disabledDate = (value) => {
    // console.log("value_"+value.valueOf()+"moment_start_"+moment("2017-11-16").valueOf())
    if (value.valueOf() > moment("2018-01-15").valueOf() || value.valueOf() < moment("2017-11-16").valueOf())
      return true;
  }

  onChangeDate = (value, dateString) => {
    

    this.props.actionStartDate(dateString[0])
    this.props.actionEndDate(dateString[1])

  }
  
  getLineChart = ()=> {
    if(this.props.selectKeyword !== ""){
      // http://161.246.35.220:9000/keyword/
      axios.post(serverUrlNew + "/keyword/", {
        "keyword": this.props.selectKeyword,
        "before": this.props.startDate,
        "after": this.props.endDate 
      }).then(data => {
        this.setState({ dataLine: data.data })
        console.log(this.state.dataLine)
      })
        .catch(function (error) {
          console.log(error);
        });
    }
  }

  getTableUser = () => {
    if(this.props.selectKeyword !== ""){
    // http://161.246.35.220:9000/keyword/user
    axios.post(serverUrlNew + "/keyword"+"/user/", {
      "keyword": this.props.selectKeyword,
      "before": this.props.startDate,
      "after": this.props.endDate 
    }).then(data => {
      this.setState({ dataTable: data.data })
      console.log(this.state.dataTable)
      this.props.actionDataUser(data.data)
     // console.log(this.props.userKeyword)
      this.dataForTable(this.state.dataTable)
    })
      .catch(function (error) {
        console.log(error);
      });
    }
  }

  dataForTable = (data) => {
    for (var i in data) {
      data[i].action = data[i].name;
      data[i].key = i;
      data[i].rank = parseInt(i);
      data[i].rank += 1;
    }
    this.setState({ formatData: data })
    console.log(this.state.formatData);
  }


  render() {
    return (
      <Layout>
        <Content style={{ margin: '0 16px' ,padding: 24}}>
          
            <Row gutter={16} style={navStyleKeyword.base}>
    <Col xs={{ span: 3 }} md={{ span: 10 }} style={{ minHeight: 30, fontSize: 32, textAlign: 'left', color: '#ffff', }}>Keyword {this.props.selectKeyword == "" ? <span /> : <span>></span>} <div className="animated slideInRight" style={navStyleKeyword.keycome}>{this.props.selectKeyword}</div></Col>
            </Row>
        {this.props.selectKeyword === "" ?  

        <Row gutter={16} type="flex" justify="space-around" align="middle" style={{ 
          marginTop:'23%',
          fontSize: 30,
          color:'lightsteelblue',
      }} >
              <Col>กรุณาเลือก Keyword ที่สนใจที่หน้า 
              <Link to="/">
                  <span> Overview</span>
                </Link></Col>
            </Row> 
            
            : 
            
            <div>
            <Row gutter={16} type="flex" justify="space-around" style={{ marginTop: '50px', marginBottom: '5px' }}>
              <Col style={navStyleKeyword.keywordlistDate}>
                <Tooltip title="เลือกวันที่หรือช่วงของวันที่ต้องการดู">
                  <div style={{ marginTop: '5px', marginBottom: '5px' }}>

                    <RangePicker
                      popupStyle={{ margin: '20px' }}
                      format="YYYY-MM-DD"
                      placeholder={['Start Time', 'End Time']}
                      onChange={this.onChangeDate}
                      defaultValue={[moment(this.props.startDate), moment(this.props.endDate)]}
                      disabledDate={this.disabledDate}
                    />
                    <Button className="go_button" onClick={this.onClick} type="primary">></Button>
                  </div>
                </Tooltip>
              </Col>
            </Row>
            
            <Row gutter={16} type="flex" justify="space-around" style={{ marginTop: '50px', marginBottom: '10px' }} >
              <Col xs={{ span: 22 }} style={{ minHeight: 360 }}>
              
              <LineChartKeyword dataLine = {this.state.dataLine}/>
              
              </Col>
            </Row>
          
          <div style={{ padding: 24, background: '#f0f2f5', minHeight: 360 }}>

            <Row gutter={16} type="flex" justify="space-around" >
              <Col xs={{ span: 16 }} style={{ minHeight: 360 }}><TableUser formatData={this.state.formatData} /></Col>
            </Row>
          </div>
    </div> }
    
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          <div className="animated fadeInUp">FlareViz Design ©2017 Created by CE6039</div>
        </Footer>
      </Layout>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  actionSelectKeyword: (data) => dispatch(actionSelectKeyword(data)),
  actionAllDate: (data) => dispatch(actionAllDate(data)),
  actionStartDate: (data) => dispatch(actionStartDate(data)),
  actionEndDate: (data) => dispatch(actionEndDate(data)),
  actionDataUser: (data) => dispatch(actionDataUser(data)),
});


const mapStateToProps = (state) => ({
  selectKeyword: state.Keeper.selectKeyword,
  allDate: state.Keeper.allDate,
  startDate: state.Keeper.startDate,
  endDate: state.Keeper.endDate,
  userKeyword: state.Keeper.userKeyword,

});

export default connect(mapStateToProps, mapDispatchToProps)(Mainkeyword);
