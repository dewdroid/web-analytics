export const actionSliderValue = (data) => (
    (dispatch) => {
        console.log('action here')
        console.log(data)
        dispatch({type: 'CHANGE_DATA' , sliderValue: data});
    }
);

export const actionSliderUser = (data) => (
    (dispatch) => {
        console.log('action here')
        console.log(data)
        dispatch({type: 'CHANGE_DATA_USER' , sliderUser: data});
    }
);

export const actionSelectKeyword = (data) => (
    (dispatch) => {
        console.log('action here2')
        console.log(data)
        dispatch({type: 'CHANGE_KEYWORD', keywordCloud: data });
    }
);

export const actionSelectUser = (data) => (
    (dispatch) => {
        console.log('action here4')
        console.log(data)
        dispatch({type: 'CHANGE_USER', getUser: data });
    }
);

export const actionAllDate = (data) => (
    (dispatch) => {
        console.log('action here5')
        console.log(data)
        dispatch({type: 'CHANGE_DATE', getAllDate: data });
    }
);

export const actionStartDate = (data) => (
    (dispatch) => {
        console.log('action here8')
        console.log(data)
        dispatch({type: 'CHANGE_DATE_START', getStartDate: data });
    }
);

export const actionEndDate = (data) => (
    (dispatch) => {
        console.log('action here7')
        console.log(data)
        dispatch({type: 'CHANGE_DATE_END', getEndDate: data });
    }
);

export const actionDataUser = (data) => (
    (dispatch) => {
        console.log('action here8')
        console.log(data)
        dispatch({type: 'CHANGE_UNAME', getDataUser: data });
    }
);

export const actionKeywordUser = (data) => (
    (dispatch) => {
        console.log('action here9')
        console.log(data)
        dispatch({type: 'CHANGE_DATA_KEYWORD_USER', getKeywordUser: data });
    }
);
