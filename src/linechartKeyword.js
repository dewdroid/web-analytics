import React, { Component } from 'react';
import 'antd/dist/antd.css';
import './css/navbar.css';
import './flareg.ico';
import moment from 'moment';
import { Layout, Menu } from 'antd';
import { Row, Col } from 'antd';
import { LineChart, Line,  Tooltip, ResponsiveContainer, CartesianGrid, XAxis, YAxis, Brush, Label } from 'recharts';


// data = [
//   { date: "startDate", frequency: number },
//   { date: "....", frequency: number },
//   { date: "....", frequency: number },
//   { date: "....", frequency: number },
//   { date: "....", frequency: number },
//   { date: "....", frequency: number },
//   { date: "endDate", frequency: number },
// ];

class LineChartKeyword extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  formatXAxis = (tickItem) => { return moment(tickItem).format('D MMM') }

  render() {
    return (
      
      <ResponsiveContainer >
        <LineChart  data={this.props.dataLine} redraw
          margin={{ top: 5, right: 20, left: 20, bottom: 5 }}>
          <XAxis dataKey="date" tickFormatter={this.formatXAxis} padding={{left: 10, right: 10}} height={60}>
          <Label  position="insideTop" offset={30} style={{ textAnchor: 'middle' }} fill="#000" >Date</Label></XAxis>
          <YAxis>
          <Label angle={270} position='left' style={{ textAnchor: 'middle' }}>
          frequency of access
    </Label>
          </YAxis>
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Line type="monotone" dataKey="frequency" stroke="#5fbff8" strokeWidth="2" activeDot={{ r: 5 }} />
          <Brush>
            <LineChart data={this.props.dataLine} redraw>
              <Line type="monotone" dataKey="frequency" stroke="#5fbff8" />
            </LineChart>
          </Brush>
        </LineChart>
      </ResponsiveContainer>
      
    );
  }
}

export default LineChartKeyword;
