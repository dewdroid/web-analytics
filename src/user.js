import React, { Component } from 'react';
import 'antd/dist/antd.css';
import './css/navbar.css';
import './flareg.ico';
import './css/user.css';
import './css/steamchartstyle.css';
import axios from 'axios';
import { serverUrlNew } from './mainpage';
import Keywordlistforuser from './keywordlistforuser';
import { Layout, Menu, Avatar } from 'antd';
import { Row, Col } from 'antd';
import { connect } from 'react-redux';
import { actionSliderValue, actionSelectKeyword, actionSelectUser, actionDataUser, actionStartDate, actionEndDate, actionKeywordUser } from './keeperAction';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

const { Content, Footer } = Layout;
const SubMenu = Menu.SubMenu;

export const navStyleUser = {
  base: {
    backgroundColor: '#eff1f4',
    marginLeft: -31,
    marginRight: -46,
    backgroundColor: '#001629',
    marginTop: -16,
    minHeight: 55,
    minWidth: 'fit-content',/* position:  absolute; */
    paddingLeft: 30,
    borderRadius: 5,
    border: '1px solid #ccc',
    transition: '2s',
  },
  keywordlist: {
    minHeight: 'auto',
    textAlign: 'center',
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
    transition: 'all 0.3s cubic-bezier(.25, .8, .25, 1)',
    padding: 20,
    marginBottom: 20,
    borderRadius: 10,
    background: '#fff',
    borderTopColor: 'coral',

  },
  usercome: {
    display: 'inline-block', color: '#5fbff8',
    textDecoration: 'underline',
    textDecorationStyle: 'dotted'
  }
};



class User extends Component {
  constructor() {
    super()
    this.state = {
      usertopList: [],
      topListTemp: [],
      userName: "",
      NewTopList: [],

    }
    
  }

  componentWillMount = () => {
    this.getUserKeyword()
    this.showUserName()
  }

  getUserKeyword = () => {
    if(this.props.selectUser !== ""){
     axios.post(serverUrlNew + "/user/keyword/", {
      "user_id": this.props.selectUser,
      "before": this.props.startDate,
      "after": this.props.endDate 
    }).then(data => {
           
      var dataRenew = data.data
      for (var i in dataRenew) {
        dataRenew[i].key = i;
        dataRenew[i].index = parseInt(i);
        dataRenew[i].index += 1;
      }
      //console.log(dataRenew)
      this.props.actionKeywordUser(dataRenew)
     // console.log(this.props.allUserKeyword)
     // console.log(this.props.sliderKeywordUser)

      var newKeyword = this.props.allUserKeyword.filter(element => (
        element.index <= this.props.sliderKeywordUser
      ))
      //console.log(newKeyword)
      this.setState({
        topListTemp: newKeyword,
        usertopList: data.data
      })
      console.log(this.state.usertopList)
    })
      .catch(function (error) {
        console.log(error);
      });

    }
  }

  getNewValuetop = (value) => {
    var NewTopList = this.props.allUserKeyword.filter(element => (
      element.index <= value
    ))
    this.setState({ topListTemp: NewTopList })

  }

  showUserName = () => {
    if(this.props.selectUser !== ''){
      console.log(this.props.userKeyword)
      for(var v in this.props.userKeyword){
        if(this.props.userKeyword[v].id === this.props.selectUser){
            var userName = this.props.userKeyword[v].name
        }
      }
      this.setState({userName: userName})
      console.log(this.state.userName)
    }
  }

    topTenkeyword = ()=> {
      var topten = this.state.usertopList.filter(element => (
        element.index <= 10
      ))
      var color = ["#3d3d3d", "#3ae374", "#ff3838", "#7158e2", "#f27c21", "#0ba2ae", "#f1c40f", "#2980b9", "#bdc3c7", "#c56cf0"]
          return topten.map((topten, index) =>
              <span key={index} style={{
                margin:5,
                padding:5,
                backgroundColor: color[index],
                color:'#fff',
                fontSize:'24px',
                borderRadius: '5px 2px 5px 2px',
                boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
              }}>{topten.value}</span>
        )
      

    }

  // dataRenew = (data) => {
  //   for (var i in data) {
  //     data[i].key = i;
  //     data[i].index = parseInt(i);
  //     data[i].index += 1;
  //   }
  //   this.props.actionKeywordUser(data)
    
  //   //console.log(data);
  //   //console.log(this.state.topListTemp);
  // }

  render() {
    console.log(this.props.startDate )
    console.log(this.props.endDate )
    return (
      <Layout>
        <Content style={{ margin: '0 16px' }}>
          <div style={{ padding: 24, background: '#f0f2f5', minHeight: 360 }}>
            <Row gutter={16} style={navStyleUser.base}>
              <Col xs={{ span: 24 }} style={{ minHeight: 30, fontSize: 32, textAlign: 'left', color: '#ffff' }}>User {this.props.selectUser === "" ? <span /> : <span>> </span>}<div className="animated slideInRight" style={navStyleUser.usercome}>{this.state.userName}</div></Col>
            </Row>
            {this.props.selectUser === "" ? <div>

              <Row gutter={16} type="flex" justify="space-around" align="middle" style={{
                marginTop: '23%',
                fontSize: 30,
                color: 'lightsteelblue',
              }} >
                <Col>กรุณาเลือก User ที่สนใจที่หน้า<Link to="/keyword">
                  <span> Observe Keyword</span>
                </Link></Col>
              </Row>
            </div>
              :
              <div>
                <Row gutter={16} type="flex" justify="space-around" style={{ marginTop: '30px', marginBottom: '10px' }} >
                  <Col xs={{ span: 24 }} style={{ minHeight: 30, fontSize: 30, textAlign: 'center' }}><div className="animated fadeInUp">USER KEYWORD</div></Col>
                </Row>
                <Row gutter={16} type="flex" justify="space-around" >
                  <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 22 }} style={{ minHeight: 150 }}><div style={navStyleUser.keywordlist}><Keywordlistforuser usertopList={this.state.topListTemp} getNewValuetop={this.getNewValuetop} /></div></Col>
                </Row>
                <Row gutter={16} type="flex" justify="space-around" style={{ marginTop: '30px', marginBottom: '10px' }} >
                  <Col xs={{ span: 24 }} style={{ minHeight: 30, fontSize: 30, textAlign: 'center' }}><div className="animated fadeInUp">{this.topTenkeyword()}</div></Col>
                </Row>
                <Row gutter={16} type="flex" justify="space-around" style={{ marginTop: '30px', marginBottom: '10px' }} >
                  <Col xs={{ span: 24 }} style={{ minHeight: 150, minWidth: '100%', textAlign: 'center' }}><iframe className="iframestyle" src={"streaming.html?" + "user=" + this.props.selectUser + "&startDate=" + this.props.startDate + "&endDate=" + this.props.endDate}></iframe></Col>
                </Row>
              </div>
            }
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          <div className="animated fadeInUp">FlareViz Design ©2017 Created by CE6039</div>
        </Footer>
      </Layout>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  actionSelectUser: (data) => dispatch(actionSelectUser(data)),
  actionSliderValue: (data) => dispatch(actionSliderValue(data)),
  actionDataUser: (data) => dispatch(actionDataUser(data)),
  actionStartDate: (data) => dispatch(actionStartDate(data)),
  actionEndDate: (data) => dispatch(actionEndDate(data)),
  actionKeywordUser: (data) => dispatch(actionKeywordUser(data)),
});


const mapStateToProps = (state) => ({
  selectUser: state.Keeper.selectUser,
  sliderKeywordUser: state.Keeper.sliderKeywordUser,
  userKeyword: state.Keeper.userKeyword,
  startDate: state.Keeper.startDate,
  endDate: state.Keeper.endDate,
  allUserKeyword: state.Keeper.allUserKeyword,
});

export default connect(mapStateToProps, mapDispatchToProps)(User);
