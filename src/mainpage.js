import React, { Component } from 'react';
import 'antd/dist/antd.css';
import './css/navbar.css';
import './flareg.ico';
import axios from 'axios';
import Chartpie from './chartpie';
import Keywordlist from './keywordlist';
import Datefilter from './datefilter';
import TableTopKey from './tabletopkey';
import Radium from 'radium';
import { Button, Layout, Row, Col, DatePicker, Tooltip, Calendar } from 'antd';
import moment from 'moment';
import { connect } from 'react-redux';
import { actionStartDate, actionEndDate, actionAllDate, actionSliderValue } from './keeperAction';

const { Content, Footer, Sider } = Layout;
const { RangePicker } = DatePicker;

export const navStyle = {
  base: {
    backgroundColor: '#eff1f4',
    marginLeft: -31,
    marginRight: -46,
    backgroundColor: '#001629',
    marginTop: -16,
    minHeight: 55,
    minWidth: 'fit-content',/* position:  absolute; */
    paddingLeft: 30,
    borderRadius: 5,
    border: '1px solid #ccc',
    transition: '2s',

  },
  keywordlistDate: {
    minHeight: 'auto',
    textAlign: 'center',
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
    transition: 'all 0.3s cubic-bezier(.25, .8, .25, 1)',
    padding: 20,
    marginBottom: 20,
    borderRadius: 10,
    background: '#48acf3',
    borderTopColor: 'coral',

  },
  keywordlistSlider: {
    minHeight: 'auto',
    textAlign: 'center',
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
    transition: 'all 0.3s cubic-bezier(.25, .8, .25, 1)',
    padding: 20,
    marginBottom: 20,
    borderRadius: 10,
    background: '#fff',
    borderTopColor: 'coral',

  },

};

export const defaultDateStart = "2017-11-16";
export const defaultDateEnd = "2018-01-15";
export const serverUrl = 'http://161.246.35.220:8000';
export const serverUrlNew = 'http://161.246.35.220:9000';

class Mainpage extends Component {
  constructor() {
    super()
    this.state = {
      topList: [],
      dataList: [],
      sites: [],
      number: 10,
      dataDate: [],
      allData: [],
      newData: 0,
      StartDate: [],
      EndDate: [],
      formatData: [],
    }
  }


  componentWillMount = () => {
    this.getValue()
    this.getDate()

  }


  onChangeDate = (value, dateString) => {
    
    this.props.actionStartDate(dateString[0])
    this.props.actionEndDate(dateString[1])
    //  console.log("StartDate: " + this.props.startDate)
    //  console.log("EndDate: " + this.props.endDate)

  }

  onClick = () => {
    // console.log("StartDate3: " + this.state.StartDate)
    // console.log("EndDate3: " + this.state.EndDate)

    //http://161.246.35.220:9000/page/top-keyword/?before=2017-11-16&after=2018-01-15
    axios.get(serverUrlNew + "/page/top-keyword/?before=" + this.props.startDate + "&after=" + this.props.endDate)
      .then(data => {
        this.setState({ allData: data.data })
        this.props.actionAllDate(data.data)
        console.log(this.props.allDate);
        var newtopList = this.state.allData.filter(element => (
          element.index < this.props.sliderKeyword
        ))
        // console.log(topList)
        this.setState({ topList: newtopList })
        this.dataForTable(this.state.allData)
      })
      this.getValue()

  }

  disabledDate = (value) => {
    // console.log("value_"+value.valueOf()+"moment_start_"+moment("2017-11-16").valueOf())
    if (value.valueOf() > moment("2018-01-15").valueOf() || value.valueOf() < moment("2017-11-16").valueOf())
      return true;
  }


  getValue = () => {
    axios.get(serverUrlNew + "/page/category-proportion/?before=" + this.props.startDate + "&after=" + this.props.endDate)
      .then(data => {
        this.setState({ dataList: data.data })
        console.log(this.state.dataList)
      }).catch(function (error) {
        console.log(error);
      });
  }

  getDate = () => {

    axios.get(serverUrlNew + "/page/top-keyword/?before=" + this.props.startDate + "&after=" + this.props.endDate)
      .then(data => {
        //this.setState({ topList: data.data })
        // this.setState({ allData: data.data})
        //console.log(this.state.allData)
        this.props.actionAllDate(data.data)
       // console.log(this.props.allDate);
        const topList = this.props.allDate.filter(element => (
          element.index < this.props.sliderKeyword
        ))
        // console.log(topList)
        
        this.setState({ topList: topList })
        this.dataForTable(this.props.allDate)
      })
  }

  getNewValuetop = (value) => {

    var NewTopList = this.props.allDate.filter(element => (
      element.index <= value
    ))
    this.setState({ topList: NewTopList })

  }

  dataForTable = (data) => {
    for (var i in data) {
      data[i].action = data[i].value;
      data[i].key = i;
      data[i].index += 1;
    }
    this.setState({ formatData: data })
   // console.log(this.state.formatData);
  }


  render() {
    return (
      <Layout >
        <Content style={{ margin: '0 16px' }}>
          <div style={{ padding: 24, background: '#f0f2f5', minHeight: 360 }}>
            <Row gutter={16} style={navStyle.base}>
              <Col xs={{ span: 10 }} md={{ span: 3 }} style={{ minHeight: 30, fontSize: 32, textAlign: 'left', color: '#ffff' }}>Overview</Col>
            </Row>

            <Row gutter={16} type="flex" justify="space-around" style={{ marginTop: '50px', marginBottom: '5px' }}>
              <Col style={navStyle.keywordlistDate}>
                <Tooltip title="เลือกวันที่หรือช่วงของวันที่ต้องการดู">
                  <div style={{ marginTop: '5px', marginBottom: '5px' }}>

                    <RangePicker
                      popupStyle={{ margin: '20px' }}
                      format="YYYY-MM-DD"
                      placeholder={['Start Time', 'End Time']}
                      onChange={this.onChangeDate}
                      defaultValue={[moment(this.props.startDate), moment(this.props.endDate)]}
                      disabledDate={this.disabledDate}
                    />
                    <Button className="go_button" onClick={this.onClick} type="primary">></Button>
                  </div>
                </Tooltip>
              </Col>
            </Row>

            <Row gutter={16} type="flex" justify="space-around" >
              <Col xs={{ span: 24 }} md={{ span: 15 }} style={navStyle.keywordlistSlider}>
                <Tooltip title="เลือกวันที่หรือช่วงของวันที่ต้องการดู">
                </Tooltip>
                <Keywordlist topList={this.state.topList} getNewValuetop={this.getNewValuetop} />
              </Col>
            </Row>

            <Row gutter={16} type="flex" justify="space-around" >
              <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 11 }} style={{ minHeight: 360, marginBottom: 20 }}><div className="recharts-wrapper2"><Chartpie dataList={this.state.dataList} /></div></Col>
              <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 11 }} style={{ minHeight: 360, textAlign: 'center' }}><TableTopKey formatData={this.state.formatData} /></Col>
            </Row>
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          <div className="animated fadeInUp">FlareViz Design ©2017 Created by CE6039</div>
        </Footer>
      </Layout>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  actionAllDate: (data) => dispatch(actionAllDate(data)),
  actionStartDate: (data) => dispatch(actionStartDate(data)),
  actionEndDate: (data) => dispatch(actionEndDate(data)),
  actionSliderValue: (data) => dispatch(actionSliderValue(data)),
});


const mapStateToProps = (state) => ({
  allDate: state.Keeper.allDate,
  startDate: state.Keeper.startDate,
  endDate: state.Keeper.endDate,
  sliderKeyword: state.Keeper.sliderKeyword,
});


export default connect(mapStateToProps, mapDispatchToProps)(Mainpage);
